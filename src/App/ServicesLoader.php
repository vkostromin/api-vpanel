<?php

namespace App;

use Silex\Application;

class ServicesLoader
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function bindServicesIntoContainer()
    {
        $this->app['variables.service'] = $this->app->share(function () {
            return new Services\VariablesService($this->app["db"]);
        });
        $this->app['params.service'] = $this->app->share(function () {
            return new Services\ParamsService($this->app["db"]);
        });
        $this->app['bitcoin.service'] = $this->app->share(function () {
            return new Services\BitcoinService($this->app["db"]);
        });
    }
}

