<?php

namespace App\Lib;

class Curl {

    private $service_url;

    public function __construct($service_url) {
        $this->service_url = $service_url;
    }

    private function init($action) {
        $curl = curl_init($this->service_url . $action);
        $headers = array(
            'Accept: application/json',
            'Content-Type: application/json',
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        return $curl;
    }

    private function process($curl) {
        $curl_response = curl_exec($curl);
        if ($curl_response !== false) {
            curl_close($curl);
            $response = json_decode($curl_response);
            return $response;
        } else {
            $info = curl_getinfo($curl);
            var_dump($info);
            curl_close($curl);
            throw new \Exception('Curl error: ' . serialize($info));
        }
    }

    public function get($action) {
        $curl = $this->init($action);
        return $this->process($curl);
    }

    public function post($action, $data) {
        $curl = $this->init($action);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        return $this->process($curl);
    }
}