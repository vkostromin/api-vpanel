<?php

namespace App\Controllers;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class ParamsController
{

    protected $currencyService;

    public function __construct($service)
    {
        $this->paramsService = $service;
    }

    public function getAll()
    {
        return new JsonResponse($this->paramsService->getAll());
    }

    public function save(Request $request)
    {
        $param = $this->getDataFromRequest($request);
        return new JsonResponse(array("id" => $this->paramsService->save($param)));
    }


    private function getDataFromRequest(Request $request)
    {
        return $param = array(
            "temperature" => $request->request->get("temperature"),
            "humidity" => $request->request->get("humidity")
        );
    }
}
