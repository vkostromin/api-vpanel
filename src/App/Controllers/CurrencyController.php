<?php

namespace App\Controllers;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class CurrencyController
{

    protected $bitcoinService;

    public function __construct($service)
    {
        $this->bitcoinService = $service;
    }

    public function getAllBitcoin()
    {
        return new JsonResponse($this->bitcoinService->getAll());
    }

    public function updateBitcoin(Request $request)
    {
        return new JsonResponse($this->bitcoinService->updateBitcoin());
    }
}
