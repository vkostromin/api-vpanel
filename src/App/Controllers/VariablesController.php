<?php

namespace App\Controllers;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class VariablesController
{

    protected $variablesService;

    public function __construct($service)
    {
        $this->variablesService = $service;
    }

    public function update($id, Request $request)
    {
        $variable = $this->getDataFromRequest($request);
        $this->variablesService->update($id, $variable);
        return new JsonResponse($variable);

    }

    public function get($id)
    {
        return new JsonResponse($this->variablesService->get($id));
    }

    private function getDataFromRequest(Request $request)
    {
        return $variable = array(
            "value" => $request->request->get("value")
        );
    }
}
