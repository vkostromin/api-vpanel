<?php

namespace App\Services;

use App\Lib;

class BitcoinService extends BaseService
{
    protected $table = 'btc_usd';

    public function getAll() {
        return $this->db->fetchAll("SELECT * FROM {$this->table} ORDER BY timestamp DESC LIMIT 100");
    }

    private function getBitcoin() {
        $curl = new Lib\Curl("http://api.coindesk.com/v1/");
        $response = $curl->get('bpi/currentprice.json');
        $data = array(
//            'timestamp': time(), //default value CURRENT_TIMESTAMP
            'updated'=> date("Y-m-d H:i:s", strtotime($response->time->updatedISO)),
            'usd'=> $response->bpi->USD->rate_float
        );
        return $data;
    }

    function updateBitcoin()
    {
        $current = $this->getBitcoin();
//        $current['timestamp'] = time(); //default value CURRENT_TIMESTAMP
        return parent::save($current);
    }

}
