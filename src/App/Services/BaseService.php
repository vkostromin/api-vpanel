<?php

namespace App\Services;

abstract class BaseService {
    protected $db;
    protected $table;

    public function __construct($db) {
        $this->db = $db;
    }

    public function getAll()
    {
        return $this->db->fetchAll("SELECT * FROM {$this->table}");
    }

    function save($param)
    {
        $this->db->insert($this->table, $param);
        return $this->db->lastInsertId();
    }

    function update($id, $param)
    {
        return $this->db->update($this->table, $param, ['id' => $id]);
    }

    function get($id)
    {
        return $this->db->fetchColumn("SELECT value FROM {$this->table} WHERE id = :id", array('id' => $id));
    }

    function delete($id)
    {
        return $this->db->delete($this->table, array("id" => $id));
    }
}
