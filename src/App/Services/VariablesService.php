<?php

namespace App\Services;

class VariablesService extends BaseService
{
    protected $table = 'variables';

    function update($id, $variable)
    {
        return $this->db->update($this->table, $variable, ['name' => $id]);
    }

    function get($id)
    {
        return $this->db->fetchAssoc("SELECT value FROM {$this->table} WHERE name = :id", array('id' => $id));
    }

}
