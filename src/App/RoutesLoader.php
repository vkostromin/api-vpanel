<?php

namespace App;

use Silex\Application;

class RoutesLoader
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->instantiateControllers();

    }

    private function instantiateControllers()
    {
        $this->app['variables.controller'] = $this->app->share(function () {
            return new Controllers\VariablesController($this->app['variables.service']);
        });
        $this->app['params.controller'] = $this->app->share(function () {
            return new Controllers\ParamsController($this->app['params.service']);
        });
        $this->app['currency.controller'] = $this->app->share(function () {
            return new Controllers\CurrencyController($this->app['bitcoin.service']);
        });
    }

    public function bindRoutesToControllers()
    {
        $api = $this->app["controllers_factory"];

//        $api->get('/variables', "variables.controller:getAll");
        $api->get('/variables/{id}', "variables.controller:get");
//        $api->post('/variables', "variables.controller:save");
//        $api->put('/variables/{id}', "variables.controller:update");
        $api->post('/variables/{id}', "variables.controller:update");
//        $api->delete('/variables/{id}', "variables.controller:delete");

        $api->get('/params', "params.controller:getAll");
        $api->post('/params/new', "params.controller:save");

        $api->get('/currency/bitcoin/update', "currency.controller:updateBitcoin");
        $api->get('/currency/bitcoin', "currency.controller:getAllBitcoin");

        $this->app->mount($this->app["api.endpoint"].'/'.$this->app["api.version"], $api);
    }
}

