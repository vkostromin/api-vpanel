-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 24, 2016 at 11:01 AM
-- Server version: 5.7.12-0ubuntu1
-- PHP Version: 7.0.4-7ubuntu2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `vpanelapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `params`
--

CREATE TABLE `params` (
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` smallint(5) UNSIGNED NOT NULL,
  `temperature` decimal(4,2) NOT NULL,
  `humidity` decimal(5,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `params`
--

INSERT INTO `params` (`timestamp`, `user_id`, `temperature`, `humidity`) VALUES
('2016-06-23 17:29:40', 1, '30.05', '70.55'),
('2016-06-23 17:29:43', 1, '30.05', '70.55'),
('2016-06-23 17:31:36', 1, '30.05', '70.55'),
('2016-06-23 17:31:39', 1, '30.05', '70.55');

-- --------------------------------------------------------

--
-- Table structure for table `variables`
--

CREATE TABLE `variables` (
  `name` varchar(128) NOT NULL DEFAULT '' COMMENT 'The name of the variable.',
  `value` varchar(2048) NOT NULL COMMENT 'The value of the variable.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Named variable/value pairs';

--
-- Dumping data for table `variables`
--

INSERT INTO `variables` (`name`, `value`) VALUES
('raspberrypi_ip', '192.168.1.101');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `params`
--
ALTER TABLE `params`
  ADD UNIQUE KEY `user_id` (`user_id`,`timestamp`);

--
-- Indexes for table `variables`
--
ALTER TABLE `variables`
  ADD PRIMARY KEY (`name`);


CREATE TABLE `btc_usd` (
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL,
  `usd` decimal(8,4) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `btc_usd`
--

INSERT INTO `btc_usd` (`timestamp`, `updated`, `usd`) VALUES
('2016-06-24 12:11:58', '2016-06-24 10:11:00', '657.2870');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `btc_usd`
--
ALTER TABLE `btc_usd`
  ADD PRIMARY KEY (`timestamp`);

