Based on a simple silex skeleton application for writing RESTful API. (https://github.com/vesparny/silex-simple-rest/releases)

**This project wants to be a starting point to writing scalable and maintainable REST api with Silex PHP micro-framework**

####How do I run it?
Run the following commands to install the php dependencies, import some data, and run a local php server.

You need at least php **5.4.***

    composer install
    mysql mydb < resources/sql/schema.sql
    php -S 0:9001 -t web/

Your api is now available at http://localhost:9001/api/v1.

####Run tests
Some tests were written, and all CRUD operations are fully tested :)

From the root folder run the following command to run tests.

    vendor/bin/phpunit


####What you will get
The api will respond to

	GET  ->   http://localhost:9001/api/v1/variables
	POST ->   http://localhost:9001/api/v1/variables
	PUT ->   http://localhost:9001/api/v1/variables/{id}
	DELETE -> http://localhost:9001/api/v1/variables/{id}

Your request should have 'Content-Type: application/json' header.
Your api is CORS compliant out of the box, so it's capable of cross-domain communication.

Try with curl:

	#GET
	curl -X GET http://localhost/~vova/test/rest/web/api/v1/variables/raspberrypi_ip -H 'Content-Type: application/json' -w "\n"
    curl -X GET http://localhost/~vova/test/rest/web/api/v1/params -H 'Content-Type: application/json' -w "\n"
    curl -X GET http://localhost/~vova/test/rest/web/api/v1/currency/bitcoin/update -H 'Content-Type: application/json' -w "\n"

    curl -X GET http://api-vpanel.rhcloud.com/api/v1/variables/raspberrypi_ip -H 'Content-Type: application/json' -w "\n"
    curl -X GET http://api-vpanel.rhcloud.com/api/v1/params -H 'Content-Type: application/json' -w "\n"
    curl -X GET http://api-vpanel.rhcloud.com/api/v1/currency/bitcoin/update -H 'Content-Type: application/json' -w "\n"

	#POST (insert)
	curl -X POST http://localhost:9001/api/v1/variables -d '{"note":"Hello World!"}' -H 'Content-Type: application/json' -w "\n"

	#PUT (update)
	curl -X POST http://localhost/~vova/test/rest/web/api/v1/variables/raspberrypi_ip -d '{"value":"192.168.1.101"}' -H 'Content-Type: application/json' -w "\n"
	curl -X POST http://localhost/~vova/test/rest/web/api/v1/params/new -d '{"temperature":30.05, "humidity": 70.55}' -H 'Content-Type: application/json' -w "\n"

	curl -X POST http://api-vpanel.rhcloud.com/api/v1/variables/raspberrypi_ip -d '{"value":"192.168.1.111"}' -H 'Content-Type: application/json' -w "\n"
	curl -X POST http://api-vpanel.rhcloud.com/api/v1/params/new -d '{"temperature":30.05, "humidity": 70.55}' -H 'Content-Type: application/json' -w "\n"

	#DELETE
	curl -X DELETE http://localhost:9001/api/v1/variables/1 -H 'Content-Type: application/json' -w "\n"







